// data katalog produk
let data_product = [
  { id: 0, nama: "Bayam", img: "img/bayam.jpg", harga: "3000", jumlah: 5 },
  { id: 1, nama: "Kubis", img: "img/kubis.jpg", harga: "2000", jumlah: 7 },
  { id: 2, nama: "Cabe", img: "img/cabe.jpg", harga: "6000", jumlah: 4 },
  { id: 3, nama: "Wortel", img: "img/wortel.jpg", harga: "4000", jumlah: 3 },
];

// menampilkan katalog produk
function displayProduct() {
  for (let i = 0; i < data_product.length; i++) {
    console.log(data_product[i]);
    let listProduct = `<div class="card" style="width: 18rem;">
    <img src="${data_product[i].img}" class="card-img-top">
    <div class="card-body">
    <h5 class="card-title"><b>${data_product[i].nama}</b></h5>
    <p class="card-text">Rp.${data_product[i].harga}/pack</p>
    <p class="card-text">per pack = 250gr</p>
    <a href="#" id="btnBeli${i}" class="btn btn-success"><i class="bi bi-cart"></i> Beli</a>
    </div>
    </div>`;
    document
      .getElementById("itemProduct")
      .insertAdjacentHTML("beforeend", listProduct);
    let btnbeli = document.getElementById(`btnBeli${i}`);
    btnbeli.addEventListener("click", () => {
      inputList(data_product[i]);
    });
  }
}

let popup = document.getElementById("popup");
let popupStok = document.getElementById("popupStok");
popup.style.display = "none";
popupStok.style.display = "none";

let listCheckout = [];
// manambahkan ke array listCheckOut
function inputList(data_product) {
  let id = data_product.id;
  let nama = data_product.nama;
  let img = data_product.img;
  let harga = data_product.harga;
  let jumlah = data_product.jumlah;
  const indexOfObject = listCheckout.findIndex((object) => {
    return object.id === id;
  });
  console.log(indexOfObject);
  if (indexOfObject !== -1) {
    // alert("sudah checkout!");
    popup.style.display = "block";
    document.getElementById("btnTutup").addEventListener("click", () => {
      popup.style.display = "none";
    });
  } else {
    let items = { id, nama, img, harga, jumlah: 1 };
    listCheckout.push(items);
    console.log(listCheckout);
    checkOut(items, jumlah);
  }
}

// menampilkan di Check Out
function checkOut(dataSayur, dataJumlah) {
  console.log("ambil item", dataSayur, "ini data jumlah stok ", dataJumlah);
  let id = dataSayur.id;
  let nama = dataSayur.nama;
  let img = dataSayur.img;
  let harga = dataSayur.harga;
  let jumlahPemesanan = dataSayur.jumlah;

  let itemProduct = `<div id="item_${id}" class="card mb-2" style="max-width: 550px;">
            <div class="row g-0">
              <div class="col-md-4">
                <img src="${img}" class="img-fluid rounded-start" style="height fit-content;">
              </div>
              <div class="col-md-8">
                <div class="card-body">
                  <h5 class="card-title">${nama}</h5>
                  <p class="card-text">Rp.${harga}/pack</p>
                  <p class="card-text"><small class="text-body-secondary">isi per pack 250gr</small></p>
                </div>
                  <div class="tombol">
                      <button onclick="deleteProduct('item_${id}',${id})" type="button" class="btn btn-danger btn-sm"><i class="bi bi-trash3"></i></button>
                      <span class="card-text" id="jumlahPemesanan_${id}"><b style='color:black'>${jumlahPemesanan}</b></span>
                      <button onclick="addProduct(${id},${dataJumlah})" type="button" class="btn btn-success btn-sm"><i class="bi bi-plus-lg"></i></button>
                  </div>
              </div>
            </div>
          </div>`;
  document
    .getElementById("detailProduct")
    .insertAdjacentHTML("beforeend", itemProduct);
  grandTotal();
}

// tambah item produk
function addProduct(id, cekStok) {
  console.log("data id dari addproduct ", id);
  const indexOfObject = listCheckout.findIndex((object) => {
    return object.id === id;
  });
  console.log("cek stok ", cekStok);
  if (listCheckout[indexOfObject].jumlah >= cekStok) {
    popupStok.style.display = "block";
    document.getElementById("btnTutupStok").addEventListener("click", () => {
      popupStok.style.display = "none";
    });
    console.log("kondisi 1", "melebihi batas stok");
  } else {
    listCheckout[indexOfObject].jumlah = listCheckout[indexOfObject].jumlah + 1;
    document.getElementById(`jumlahPemesanan_${id}`).innerHTML =
      listCheckout[indexOfObject].jumlah;
  }
  grandTotal();
  // totalSub();
}

let totalOrder = [];
function grandTotal() {
  let grandTotal = 0;
  for (let i = 0; i < listCheckout.length; i++) {
    grandTotal += listCheckout[i].jumlah * listCheckout[i].harga;
  }
  document.getElementById("grandTotal").innerHTML = "Total : Rp." + grandTotal;
  totalOrder.push(grandTotal);
}

// hapus item produk
function deleteProduct(item_id, id) {
  // hapus div
  let item = document.getElementById(item_id);
  console.log(item);
  item.remove();
  // hapus isi array
  const indexOfObject = listCheckout.findIndex((object) => {
    return object.id === id;
  });
  listCheckout.splice(indexOfObject, 1);
  grandTotal();

  document.getElementById(`detailPayment`).innerText = "";
}

function bayar() {
  document.getElementById("logoutBtn").style.display = "none";
  document.getElementById("backBtn").style.display = "block";
  document.getElementById("displayProduct").style.display = "none";
  document.getElementById("fakturOrder").style.display = "block";
  let detail, harga, qty;
  let sum = 0;
  for (let i = 0; i < listCheckout.length; i++) {
    sum = `<td>Rp.${listCheckout[i].jumlah * listCheckout[i].harga}</td>`;
    detail = `<td>${[i + 1]}. ${listCheckout[i].nama} </td>`;
    harga = `<td>Rp.${listCheckout[i].harga}/pack</td>`;
    qty = `<td>${listCheckout[i].jumlah} pack</td>`;
    console.log(detail);
    document
      .getElementById("rincianPesanan")
      .insertAdjacentHTML("beforeend", detail + "<br>");
    document
      .getElementById("sum")
      .insertAdjacentHTML("beforeend", sum + "<br>");
    document
      .getElementById("harga")
      .insertAdjacentHTML("beforeend", harga + "<br>");
    document
      .getElementById("qty")
      .insertAdjacentHTML("beforeend", qty + "<br>");
  }

  document.getElementById("rincianTotal").innerHTML =
    "<b style='color:black';>Total seluruh pembelian Rp. " +
    totalOrder[totalOrder.length - 1] +
    "</b>";
}

function back() {
  document.getElementById("detailProduct").innerHTML = "";
  document.getElementById("grandTotal").innerHTML = "";
  document.getElementById("logoutBtn").style.display = "block";
  document.getElementById("backBtn").style.display = "none";
  document.getElementById("displayProduct").style.display = "block";
  document.getElementById("fakturOrder").style.display = "none";
  document.getElementById("rincianPesanan").innerHTML = "";
  document.getElementById("rincianTotal").innerHTML = "";
  document.getElementById("harga").innerHTML = "";
  document.getElementById("qty").innerHTML = "";
  document.getElementById("sum").innerHTML = "";
  listCheckout = [];
  totalOrder = [];
}
